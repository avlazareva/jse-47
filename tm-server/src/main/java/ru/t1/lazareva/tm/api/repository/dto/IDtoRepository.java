package ru.t1.lazareva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;

import java.util.Comparator;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDto> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    boolean existsByIndex(@NotNull Integer id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@NotNull final Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    int getSize() throws Exception;

    void remove(@NotNull M model) throws Exception;

    M update(@NotNull M model) throws Exception;

}