package ru.t1.lazareva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password, @Nullable Role role) throws Exception;

    @Nullable
    UserDto findByLogin(@NotNull String login) throws Exception;

    @Nullable
    UserDto findByEmail(@NotNull String email) throws Exception;

    Boolean isLoginExists(@NotNull String login) throws Exception;

    Boolean isEmailExists(@NotNull String email) throws Exception;

}
